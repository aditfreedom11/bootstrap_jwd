<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Aditya Aziz Fikhri</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex flex-column">
          <div class="image mx-auto d-block">
            <img src="dist/img/pp.png" class="img-circle elevation-2 mx-auto" alt="User Image">
          </div>
          <div class="d-flex flex-column justify-content-center mt-2 mx-auto">
            <h3 class="text-light font-weight-bold justify-content-center mx-auto">Aditya Aziz Fikhri</h3>
            <a class="btn btn-warning text-dark" aria-pressed="false">Full Stack Web Developer</a>
          </div>
          <div class="text-wrap container mt-3">
            <p class="text-light text-left">
              "Saya memiliki pengalaman 5 tahun dalam mengerjakan projek aplikasi berbasis web PHP khususnya menggunakan
              framework Laravel dan Codeigniter."
            </p>
          </div>
          <div class="d-flex justify-content-center">
            <a href="dist/documents/cv_adit.pdf" class="btn btn-success text-light font-weight-bold">Download CV Saya Disini</a>
          </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
              <a href="index.php" class="nav-link active">
                <i class="nav-icon fa fa-home"></i>
                <p>
                  Beranda
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="livesearch.php" class="nav-link">
                <i class="nav-icon fa fa-database"></i>
                <p>
                  Program LiveSearch
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="pengalaman_kerja.html" class="nav-link">
                <i class="nav-icon fa fa-user"></i>
                <p>
                  Pengalaman Kerja
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="galery_foto.html" class="nav-link">
                <i class="nav-icon far fa-image"></i>
                <p>
                  Galeri Foto
                </p>
              </a>
            </li>

          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Beranda</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Aditya Aziz Fikhri</a></li>
                <li class="breadcrumb-item active">Beranda</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
        <hr class="text-black w-100 h-20">

      </div>

      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div id="carouselExampleIndicators" class="carousel slide bg-dark mx-auto d-block" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner mx-auto d-block" style="width: 80%;background-size: cover;">
              <div class="carousel-item active " style="background-size: cover;">
                <img class="d-block w-100" src="./dist/img/Screenshot from 2023-04-17 21-17-12.png" alt="First slide">
                <div class="carousel-caption d-none d-md-block">
                  <h5 class="font-weight-bold">PPDB ONLINE</h5>
                  <p class="">Penerimaan Peserta Didik Baru Kabupaten Bireuen</p>
                </div>
              </div>
              <div class="carousel-item" style="background-size: cover;">
                <img class="d-block w-100" src="./dist/img/Screenshot from 2023-04-19 13-41-58.png" alt="Second slide">
                <div class="carousel-caption d-none d-md-block">
                  <h5 class="font-weight-bold text-dark">Infosiswa</h5>
                  <p class="text-dark">Infosiswa Sekolah Sukma Bangsa Bireuen</p>
                </div>
              </div>
              <div class="carousel-item" style="background-size: cover;">
                <img class="d-block w-100" src="./dist/img/Screenshot from 2023-04-28 16-56-03.png" alt="Third slide">
                <div class="carousel-caption d-none d-md-block">
                  <h5 class="font-weight-bold text-danger">Weekly Folder</h5>
                  <p class="">Weekly Folder Sekolah Sukma Bireuen</p>
                </div>
              </div>
              <div class="carousel-item" style="background-size: cover;">
                <img class="d-block w-100" src="./dist/img/Screenshot from 2023-04-17 21-05-25.png" alt="Fourth slide">
                <div class="carousel-caption d-none d-md-block">
                  <h5 class="font-weight-bold">SIM BPP</h5>
                  <p class="">SIM BPP Sekolah Sukma Bireuen</p>
                </div>
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
          <div class="d-flex justify-content-center flex-column">
            <h2 class="p-2 font-weight-bold mx-auto mt-4">Apa Yang Saya Lakukan?</h2>
            <hr class="text-black w-100 h-20">
            <div class="d-flex flex-column justify-content-center text-justify">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi laborum eum fuga, nihil ea corrupti, aliquam rem esse repellat illum doloribus provident iusto ut quia laudantium molestiae fugiat minima? Inventore voluptas hic officiis quae voluptate eius itaque quo consectetur officia voluptatum qui accusamus tempore odit, laudantium corporis eveniet beatae. Repudiandae maiores libero, facere nostrum explicabo repellat ipsa, tempora reiciendis qui labore veritatis similique at neque autem iusto vitae! Nulla iusto cum modi inventore repudiandae animi voluptas porro eveniet repellat quis ullam, odit accusantium. Error, alias ex, iste ad voluptatibus pariatur eveniet necessitatibus rem saepe velit eum doloribus quae maiores quis fuga. Iusto, quasi. Quasi maiores eius dolor aliquid rem id placeat veniam reprehenderit delectus odit, nisi reiciendis enim fugiat, quis nam explicabo assumenda molestias adipisci sunt quo. Sunt fuga earum architecto voluptas enim recusandae impedit dolore aut distinctio. Recusandae eius perspiciatis quo vero non temporibus earum vitae, accusantium illum facere, quibusdam voluptate aspernatur, deleniti quasi cum eligendi fugiat quis vel veritatis voluptatibus! Delectus quaerat ipsam laudantium nulla perspiciatis nesciunt distinctio exercitationem fuga blanditiis debitis molestiae, dolorum earum. Vero autem sint tenetur omnis ex doloremque quis perspiciatis iste aperiam quidem repellendus perferendis, alias itaque voluptas eos totam nam voluptate hic odit.
            </div>
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer mt-4">
      <strong>Copyright &copy; 2023 <a href="#">Aditya Aziz Fikhri</a>.</strong>
      All rights reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.js"></script>
  <script src="dist/js/pages/dashboard.js"></script>
</body>

</html>