<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Aditya Aziz Fikhri</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

  <link rel="stylesheet" href="https://cdn.datatables.net/1.13.5/css/jquery.dataTables.css" />

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>

</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex flex-column">
          <div class="image mx-auto d-block">
            <img src="dist/img/pp.png" class="img-circle elevation-2 mx-auto" alt="User Image">
          </div>
          <div class="d-flex flex-column justify-content-center mt-2 mx-auto">
            <h3 class="text-light font-weight-bold justify-content-center mx-auto">Aditya Aziz Fikhri</h3>
            <a class="btn btn-warning text-dark" aria-pressed="false">Full Stack Web Developer</a>
          </div>
          <div class="text-wrap container mt-3">
            <p class="text-light text-left">
              "Saya memiliki pengalaman 5 tahun dalam mengerjakan projek aplikasi berbasis web PHP khususnya menggunakan
              framework Laravel dan Codeigniter."
            </p>
          </div>
          <div class="d-flex justify-content-center">
            <a href="dist/documents/cv_adit.pdf" class="btn btn-success text-light font-weight-bold">Download CV Saya Disini</a>
          </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
              <a href="index.php" class="nav-link">
                <i class="nav-icon fa fa-home"></i>
                <p>
                  Beranda
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="livesearch.php" class="nav-link active">
                <i class="nav-icon fa fa-database"></i>
                <p>
                  Program LiveSearch
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="pengalaman_kerja.html" class="nav-link">
                <i class="nav-icon fa fa-user"></i>
                <p>
                  Pengalaman Kerja
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="galery_foto.html" class="nav-link">
                <i class="nav-icon far fa-image"></i>
                <p>
                  Galeri Foto
                </p>
              </a>
            </li>

          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Program LiveSearch</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Aditya Aziz Fikhri</a></li>
                <li class="breadcrumb-item active">LiveSearch</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
        <hr class="text-black w-100 h-20">

      </div>

      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <?php
          $nama_database = "data_brg";
          ///////////////////////////////////////////////////////////////
          //Koneksi MySQL
          $conn = mysqli_connect("localhost", "root", "");
          // Cek Koneksi
          if (mysqli_connect_errno()) {
            echo "Koneksi database gagal : " . mysqli_connect_error();
          }

          //Membuat Database data_brg, comment bila sudah terbuat
          $sql = "CREATE DATABASE IF NOT EXISTS data_brg ";
          if ($conn->query($sql) === FALSE) {
            echo "Gagal Membuat Database : " . $conn->error;
          }
          ///////////////////////////////////////////////////////////////


          //Koneksi ke Database data_brg
          $conn2 = mysqli_connect("localhost", "root", "", $nama_database);

          //Membuat table brg, comment bila sudah terbuat
          $sql2 = "CREATE TABLE IF NOT EXISTS brg(id INT PRIMARY KEY AUTO_INCREMENT,nama_brg varchar(255), jenis varchar(255), jumlah varchar(10), sts_brg varchar(255))";
          if ($conn2->query($sql2) === FALSE) {
            echo "<br>Gagal Membuat Tabel : " . $conn->error;
          }
          ?>

          <div class="shadow p-4 mb-4 bg-white rounded">
            <h2 class="font-weight-bold">Input Data</h2>
            <hr>
            <form action="livesearch.php" method="POST">
              <div class="form-group">
                <label>Nama Barang</label>
                <input type="text" name="nambar" class="form-control" placeholder="Nama barang" required autofocus>
              </div>
              <div class="form-group">
                <label>Kategori Barang</label>
                <select name="jenis" class="form-control" required>
                  <option value="Furniture">Furniture</option>
                  <option value="Glassware">Glassware</option>
                  <option value="Electronic">Electronic</option>
                  <option value="Mechanic">Mechanic</option>
                </select>
              </div>
              <div class="form-group">
                <label>Jumlah</label>
                <input type="number" name="jumlah" class="form-control" placeholder="Jumlah barang" required autofocus>
              </div>
              <div class="form-group">
                <label>Status</label>
                <select name="sts_brg" class="form-control" required>
                  <option value="Ready">Ready</option>
                  <option value="Indent">Indent</option>
                  <option value="Innactive">Innactive</option>
                </select>
              </div>
              <div class="form-group">
                <input class="btn btn-primary font-weight-bold form-control" type="submit" name="submit" value="Submit">
              </div>
            </form>
          </div>
          <div class="shadow p-4 bg-white rounded">
            <h2 class="font-weight-bold">Data Barang</h2>
            <hr>
            <table class="table table-striped table-bordered text-center table-sm mt-2 table-responsive-sm" id="tabel_brg">
              <thead>
                <tr class="bg-dark text-light">
                  <th>No</th>
                  <th>Nama Barang</th>
                  <th>Jenis</th>
                  <th>Jumlah</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $query_tampil = mysqli_query($conn2, "SELECT * FROM brg");
                $no = 1;
                while ($data = mysqli_fetch_assoc($query_tampil)) {
                ?>
                  <tr>
                    <td class="align-middle"><b><?php echo $no++; ?></b></td>
                    <td class="align-middle"><?php echo $data['nama_brg']; ?></td>
                    <td class="align-middle"><?php echo $data['jenis']; ?></td>
                    <td class="align-middle"><?php echo $data['jumlah']; ?></td>
                    <?php
                      if ($data['sts_brg']=="Ready") {
                        $class = "btn btn-success";
                      }else if($data['sts_brg']=="Indent"){
                        $class = "btn btn-warning";
                      }else{
                        $class = "btn btn-danger";
                      }
                    ?>
                    <td class="align-middle"><a class="<?php echo $class?> rounded-pill"><b><?php echo $data['sts_brg']; ?></b></a></td>
                    <td class="align-middle">
                      <a class="btn btn-info font-weight-bold btn-sm" onclick="maintenance()" href="#"><i class="fa fa-edit"></i></a>
                      <a class="btn btn-danger font-weight-bold btn-sm" onclick="maintenance()" href="#"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <?php
          //insert data
          if (isset($_POST['submit'])) {
            $nambar = $_POST['nambar'];
            $jenis = $_POST['jenis'];
            $jumlah = $_POST['jumlah'];
            $sts_brg = $_POST['sts_brg'];
            $query_insert = mysqli_query($conn2, "INSERT INTO brg (id, nama_brg, jenis, jumlah, sts_brg) VALUES(null,'$nambar','$jenis','$jumlah','$sts_brg')");
            var_dump($query_insert);
            echo '<script>alert("Data Berhasil Ditambahkan")</script>';
            echo '<script>window.location.href = "livesearch.php";</script>';
          }
          ?>
        </div>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer mt-4">
    <strong>Copyright &copy; 2023 <a href="#">Aditya Aziz Fikhri</a>.</strong>
    All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  </div>
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.js"></script>
  <script src="dist/js/pages/dashboard.js"></script>
  <script>
    $(document).ready(function() {
      $('#tabel_brg').DataTable();
    });
  </script>
  <script>
    function maintenance() {
      alert(`Sedang Dalam Pengembangan`);
    }
  </script>
</body>

</html>